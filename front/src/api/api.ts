import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { Submission } from "./submissionsService";

export const api = createApi({
    reducerPath: 'api',
    baseQuery: fetchBaseQuery({
         baseUrl: "http://localhost:8080/api/v1"
    }),
    endpoints: (builder) => ({
        createStudent: builder.mutation<Submission, {
            readonly isuId: number
            readonly name: string
            readonly surname: string
        }>({
            query: ( body ) => ({
                url: '/submissions',
                method: 'POST',
                body,
            }),
            invalidatesTags: ['Submission']
        }),
    }),
    tagTypes: ['Submission']
})

