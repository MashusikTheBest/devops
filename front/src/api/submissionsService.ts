import {api} from "./api";

export type Submission = {
    id: number,
    studentName: string,
    text: string,
    category: string
}


export const submissionService = api.injectEndpoints({
    endpoints: builder => ({
        createSubmission: builder.mutation<Submission, {
            studentName: string,
            text: string,
            category: string
        }>({
            query: ( body ) => ({
                url: '/submissions',
                method: 'POST',
                body,
            }),
            invalidatesTags: ['Submission']
        }),

        deleteSubmission: builder.mutation<void, number>({
            query: ( id ) => ({
                url: `/submissions/${id}`,
                method: 'DELETE',
            }),
            invalidatesTags: ['Submission']
        }),

        getSubmissions: builder.query<Submission[], void>({
            query: () => ({
                url: '/submissions',
            }),
            providesTags: ['Submission']
        })
    })
})
