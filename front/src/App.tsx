import React from 'react';
import logo from './logo.svg';
import './App.css';
import './pages/SubmissionsPageContainer'
import {SubmissionsPageContainer} from "./pages/SubmissionsPageContainer";
import {Provider} from "react-redux";
import {store} from "./api/store";

function App() {
  return (
    <div className="App">
      <div className="content">
           <Provider store={ store }>
               <SubmissionsPageContainer/>
      </Provider>

      </div>
    </div>
  );
}

export default App;
