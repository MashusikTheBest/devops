import {useState} from "react";
import {submissionService} from "../api/submissionsService";
import './page.css'


export const SubmissionsPageContainer = () => {
    return <div className="content">
        <SubmissionsList />
        <CreateSubmissionForm />
    </div>
}


const CreateSubmissionForm = () => {
    const [ studentName, setStudentName ] = useState('')
    const [ text, setText ] = useState('')
    const [ category, setCategory ] = useState('')

    const [ createSubmission ] = submissionService.useCreateSubmissionMutation()



    const onCreate = () => {
        createSubmission({
            studentName,
            text,
            category
        })
    }

    return <div className="form-wrapper">
        <div className="form-title">
            <img src="/title.png"/>
        </div>
        <div className="form-content">
            <div className="form-field">
                <label htmlFor="studentName">Student Name:</label>
                <input id="studentName"
                       type="text"
                       value={ studentName }
                       onChange={ e => setStudentName(e.target.value) }
                />
            </div>
            <div className="form-field">
                <label htmlFor="text">Text:</label>
                <input id="text"
                       value={ text }
                       onChange={ e => setText(e.target.value) }
                />
            </div>
            <div className="form-field">
                <label htmlFor="category">Category:</label>
                <input id="surname"
                       value={ category }
                       onChange={ e => setCategory(e.target.value) }
                />
            </div>
        </div>
        <button className="form-button" onClick={ onCreate }>Create</button>
    </div>
}


const SubmissionsList = () => {
    const { data: submissions } = submissionService.useGetSubmissionsQuery()

    return <div className="sub-table-wrapper">
        <table className="sub-table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Student Name</th>
                <th>Text</th>
                <th>Category</th>
            </tr>
            </thead>
            <tbody>
            {
                submissions?.map(s => <tr key={ s.id }>
                    <td>{s.id}</td>
                    <td>{s.studentName}</td>
                    <td>{s.text}</td>
                    <td>{s.category}</td>
                </tr>)
            }
            </tbody>
        </table>
    </div>
}
