package com.tuneit.article.submission_service

import com.tuneit.article.submission_service.http.controller.dto.Category
import com.tuneit.article.submission_service.model.Submission
import com.tuneit.article.submission_service.repo.storage.SubmissionStorage
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.longs.shouldBeExactly


class SubmissionTest : DescribeSpec({
    describe("First test") {
        it("Check first case") {
            val submissionStorage = SubmissionStorage()
            submissionStorage.init()
            val submission = Submission(
                id = 0L,
                studentName = "Tsopa",
                text = "Masha",
                category = Category.DEVOPS
            )
            val saved = submissionStorage.save(submission)
            saved.id.shouldBeExactly(1L)
        }
    }
})