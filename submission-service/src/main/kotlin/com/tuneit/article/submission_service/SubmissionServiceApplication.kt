package com.tuneit.article.submission_service

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SubmissionServiceApplication

fun main(args: Array<String>) {
	runApplication<SubmissionServiceApplication>(*args)
}
