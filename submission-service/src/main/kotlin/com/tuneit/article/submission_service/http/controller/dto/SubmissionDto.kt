package com.tuneit.article.submission_service.http.controller.dto

enum class Category {
    MATH,
    PROGRAMING,
    DEVOPS
}

data class SubmissionDto(
    val studentName: String,
    val text: String,
    val category: Category
)
