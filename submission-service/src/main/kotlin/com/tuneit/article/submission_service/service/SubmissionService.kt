package com.tuneit.article.submission_service.service

import com.tuneit.article.submission_service.exceptions.SubmissionNotFoundException
import com.tuneit.article.submission_service.http.controller.dto.SubmissionDto
import com.tuneit.article.submission_service.model.Submission
import com.tuneit.article.submission_service.repo.SubmissionRepository
import org.springframework.stereotype.Service

@Service
class SubmissionService(
    private val submissionRepository: SubmissionRepository
) {

    fun createSubmission(submissionDto: SubmissionDto):Submission {
        return submissionRepository.createSubmission(
            Submission(
                id = 0L,
                studentName = submissionDto.studentName,
                text = submissionDto.text,
                category = submissionDto.category
            )
        )
    }


    fun findSubmissionById(id: Long): Submission {
        return submissionRepository.findSubmissionById(id)
            ?: throw SubmissionNotFoundException("No submission with id: $id")
    }

    fun deleteSubmissionById(id: Long) {
        return submissionRepository.deleteSubmissionById(id)
    }

    fun getAllSubmissions(): List<Submission> {
        return submissionRepository.getAllSubmissions()
    }

    fun updateSubmission(id: Long, submissionDto: SubmissionDto): Submission {
        return submissionRepository.createSubmission(Submission(
            id = id,
            studentName = submissionDto.studentName,
            text = submissionDto.text,
            category = submissionDto.category
        ))
    }
}