package com.tuneit.article.submission_service.http.controller

import com.tuneit.article.submission_service.http.controller.dto.SubmissionDto
import com.tuneit.article.submission_service.model.Submission
import com.tuneit.article.submission_service.service.SubmissionService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/submissions")
@CrossOrigin
class SubmissionController(
    val submissionService: SubmissionService
) {

    @PostMapping
    fun createSubmission(
        @RequestBody submissionDto: SubmissionDto
    ): ResponseEntity<Submission> {
        return ResponseEntity.status(HttpStatus.CREATED).body(submissionService.createSubmission(submissionDto))

    }

    @PutMapping("/{id}")
    fun updateSubmission(
        @PathVariable id: Long,
        @RequestBody submissionDto: SubmissionDto
    ) :ResponseEntity<Submission> {
        return ResponseEntity.ok(submissionService.updateSubmission(id, submissionDto))
    }

    @DeleteMapping("/{id}")
    fun deleteSubmission(
        @PathVariable id: Long
    ): ResponseEntity<Any> {
        submissionService.deleteSubmissionById(id)
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build()
    }

    @GetMapping("/{id}")
    fun getSubmissionById(
        @PathVariable id: Long
    ): ResponseEntity<Submission> {
        return ResponseEntity.ok(submissionService.findSubmissionById(id))
    }

    @GetMapping
    fun getAllSubmissions() : ResponseEntity<List<Submission>> {
        return ResponseEntity.ok(submissionService.getAllSubmissions())
    }

}