package com.tuneit.article.submission_service.model

import com.tuneit.article.submission_service.http.controller.dto.Category

data class Submission (
    var id: Long,
    val studentName: String,
    val text: String,
    val category: Category
)
