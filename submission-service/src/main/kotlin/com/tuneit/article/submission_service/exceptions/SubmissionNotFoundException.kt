package com.tuneit.article.submission_service.exceptions

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

class SubmissionNotFoundException(message: String? = null) : RuntimeException(message)

class ErrorResponse(
    val type: ErrorType,
    val message: String,
)

enum class ErrorType {
    NOT_FOUND
}

@ControllerAdvice
class SubmissionExceptionHandler {

    @ExceptionHandler
    fun handelNotFound(e: SubmissionNotFoundException): ResponseEntity<ErrorResponse> =
        ResponseEntity.badRequest().body(
            ErrorResponse(
                ErrorType.NOT_FOUND,
                e.message ?: "not found exception"
            )
        )
}