package com.tuneit.article.submission_service.repo

import com.tuneit.article.submission_service.model.Submission
import com.tuneit.article.submission_service.repo.storage.SubmissionStorage
import org.springframework.stereotype.Repository

@Repository
class SubmissionRepository(
    private val submissionStorage: SubmissionStorage
) {

    fun createSubmission(submission: Submission): Submission {
        return submissionStorage.save(submission)
    }

    fun findSubmissionById(id: Long): Submission? {
        return submissionStorage.findById(id)
    }

    fun deleteSubmissionById(id: Long) {
        return submissionStorage.deleteById(id)
    }

    fun getAllSubmissions(): List<Submission> {
        return submissionStorage.getAll()
    }

    fun updateSubmission(submission: Submission): Submission {
        return submissionStorage.save(submission)
    }
}