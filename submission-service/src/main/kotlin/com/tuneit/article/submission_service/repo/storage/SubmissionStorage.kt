package com.tuneit.article.submission_service.repo.storage

import com.tuneit.article.submission_service.model.Submission
import jakarta.annotation.PostConstruct
import org.springframework.stereotype.Component
import java.util.concurrent.locks.ReadWriteLock
import java.util.concurrent.locks.ReentrantReadWriteLock

@Component
class SubmissionStorage {
    private lateinit var storage: MutableMap<Long, Submission>
    private lateinit var lock: ReadWriteLock
    private var identityGeneration = 1L

    @PostConstruct
    fun init() {
        storage = HashMap()
        lock = ReentrantReadWriteLock()
    }


    fun save(submission: Submission): Submission {
        lock.writeLock().lock()
        if (submission.id == 0L) {
            submission.id = identityGeneration++
        }
        storage[submission.id] = submission
        lock.writeLock().unlock()
        return submission
    }

    fun findById(id: Long): Submission? {
        val submission: Submission?
        lock.readLock().lock()
        submission = storage[id]
        lock.readLock().unlock()
        return submission
    }

    fun deleteById(id: Long) {
        lock.writeLock().lock()
        storage.remove(id)
        lock.writeLock().unlock()
    }

    fun getAll(): List<Submission> {
        val result: List<Submission>
        lock.readLock().lock()
        result = storage.values.toList()
        lock.readLock().unlock()
        return result
    }
}